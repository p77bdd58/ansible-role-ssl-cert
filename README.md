ssl-cert
========

Generate self signed cert, key & csr.


Requirements
------------


Role Variables
--------------

```
ssl_cert_country: 'AU'
ssl_cert_locality: 'Gold Coast'
ssl_cert_organization: 'Ben Cooling Sole Trader'
ssl_cert_state: 'Gold Coast'
ssl_cert_common_name: '{{ansible_fqdn}}'
ssl_cert_days: '365'
ssl_cert_path: '{{playbook_dir}}/ssl'
ssl_cert_name: '{{ssl_cert_common_name}}'
ssl_cert_generate_self_signed: true
ssl_cert_key_size: '2048'
```


Dependencies
------------

No dependencies.


Example Playbook
----------------

```
# test playbook
---
- hosts: server
  roles:
    - { role: ssl-cert,
        ssl_cert_name: 'hello_world',
        ssl_cert_country: 'AU',
        ssl_cert_locality: 'Gold Coast',
        ssl_cert_organization: 'bencooling',
        ssl_cert_state: 'Queensland'
      }
```

License
-------

BSD

Author Information
------------------

[bcooling.com.au](bcooling.com.au)
